# pic-test
Position independent code test

## How I fixed my linker errors related to the reallocation?
**TL;DR**
See commit [a72112e3](https://gitlab.com/mtyszczak/pic-test/-/commit/a72112e3a506b749bf83b04a1f20fc95a46c22a3)

### Usage of the `POSITION_INDEPENDENT_CODE` target property
Every target has its `POSITION_INDEPENDENT_CODE` property set, wchich defaults to `ON` when building shared library and `OFF` when building static library.
Due to that it is not recommended to pass `-fPIC` option directly to the `CMAKE_CXX_OPTIONS` as the CMake behaviour (generated compiler options) may be unexpected.

You can safely override PIC property using `CMAKE_POSITION_INDEPENDENT_CODE` option:
```cmake
set( CMAKE_POSITION_INDEPENDENT_CODE [ON/OFF] )
```
, but provided value will be used for every target in your project.

If you want to set PIC property only for a given target use `set_property` CMake function like this:
```cmake
set_property( TARGET <LIB_NAME> PROPERTY POSITION_INDEPENDENT_CODE [ON/OFF] )
```

If you want to read more about the `POSITION_INDEPENDENT_CODE` cmake property, there is a great comment on GitHub explaining the current CMake behaviour:
https://github.com/arsenm/sanitizers-cmake/issues/13#issuecomment-315532253

## Building
### Building on Ubuntu 18.04/20.04
For Ubuntu 18.04/20.04 users, after installing the right packages with apt, pic-test's programs will build out of the box without further effort:
```bash
sudo apt update

# Install required compilation tools
sudo apt install -y \
    cmake \
    g++ \
    git \
    make

git clone https://gitlab.com/mtyszczak/pic-test.git
cd pic-test
git checkout master
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) all
# optional
make install  # defaults to /usr/local
```

## License
See [LICENSE.md](LICENSE.md)
