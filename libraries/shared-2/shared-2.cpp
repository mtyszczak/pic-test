#include <shared-2.hpp>

#include <registrar.hpp>

#include <static-2.hpp>

#include <shared-1.hpp>

REGISTRAR_IMPLEMENT( shared, 2,
  REGISTRAR_INVOKE( shared, 1 );
  REGISTRAR_INVOKE( static, 2 );
)
