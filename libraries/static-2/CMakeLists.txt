file(GLOB HEADERS "include/*.hpp")

add_library( static-2 STATIC static-2.cpp ${HEADERS} )

set_property( TARGET static-2 PROPERTY POSITION_INDEPENDENT_CODE ON )

target_link_libraries( static-2 PUBLIC registrar )

target_include_directories( static-2 PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" )
