#pragma once

#include <iostream>

#define REGISTRAR_REGISTER( type, index, ... )  \
  namespace registrar {                         \
    void using_## type ##_## index ##_invoke(); \
  }

#define REGISTRAR_IMPLEMENT_BEGIN( type, index ) \
  namespace registrar {                          \
    void using_## type ##_## index ##_invoke()   \
    { /* Users here can create inner scope */    \
      static const char* const __type__ = #type; \
      static const size_t __index__ = index;

#define REGISTRAR_IMPLEMENT_END()                                                                   \
      std::cout << "Registrar of type <" << __type__ << ">#" << __index__ << " has been invoked\n"; \
    }                                                                                               \
  }

#define REGISTRAR_IMPLEMENT( type, index, ... ) \
  REGISTRAR_IMPLEMENT_BEGIN(type, index)        \
  {                                             \
    __VA_ARGS__;                                \
  }                                             \
  REGISTRAR_IMPLEMENT_END()

#define REGISTRAR_INVOKE( type, index ) \
  registrar::using_## type ##_## index ##_invoke()
