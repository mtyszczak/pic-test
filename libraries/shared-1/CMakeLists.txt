file(GLOB HEADERS "include/*.hpp")

add_library( shared-1 SHARED shared-1.cpp ${HEADERS} )

target_link_libraries( shared-1 PUBLIC registrar static-1 )

target_include_directories( shared-1 PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" )
