#include <shared-1.hpp>

#include <registrar.hpp>

#include <static-1.hpp>

REGISTRAR_IMPLEMENT( shared, 1,
  REGISTRAR_INVOKE( static, 1 );
)
